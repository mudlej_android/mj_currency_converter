package com.gitlab.mudlej.mj_currency_converter.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.mudlej.mj_currency_converter.data.models.Rates
import com.gitlab.mudlej.mj_currency_converter.main.util.DispatcherProvider
import com.gitlab.mudlej.mj_currency_converter.main.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: MainRepository,
    private val dispatchers: DispatcherProvider
): ViewModel() {

    sealed class CurrencyEvent {
        class Success(val resultText: String) : CurrencyEvent()
        class Failure(val errorText: String) : CurrencyEvent()
        object Loading : CurrencyEvent()
        object Empty : CurrencyEvent()
    }

    private val _conversion = MutableStateFlow<CurrencyEvent>(CurrencyEvent.Empty)
    val conversion: StateFlow<CurrencyEvent> = _conversion

    fun convert(amountStr: String, fromCurrency: String, toCurrency: String) {
        val fromAmount = amountStr.toFloatOrNull()
        if (fromAmount == null) {
            _conversion.value = CurrencyEvent.Failure("Not a valid amount")
            return
        }

        viewModelScope.launch(dispatchers.io) {
            _conversion.value = CurrencyEvent.Loading
            when(val ratesResponse = repository.getRates(fromCurrency)) {
                is Resource.Error -> _conversion.value = CurrencyEvent.Failure(ratesResponse.message!!)
                is Resource.Success -> {
                    val rates = ratesResponse.data!!.rates
                    val rate = getRateForCurrency(toCurrency, rates)

                    if (rate == null) {
                        _conversion.value = CurrencyEvent.Failure("Unexpected error")
                    } else {
                        val convertedCurrency = "%.2f".format(fromAmount * rate)
                        _conversion.value = CurrencyEvent.Success(
                            "$convertedCurrency $toCurrency"
                        )
                    }
                }
            }
        }
    }

    // This is a ridiculous function, I just followed the tutorial here
    private fun getRateForCurrency(toCurrency: String, rates: Rates) = when (toCurrency) {
        "ADA" -> rates.ADA
        "AED" -> rates.AED
        "AUD" -> rates.AUD
        "BAT" -> rates.BAT
        "BDT" -> rates.BDT
        "BGN" -> rates.BGN
        "BIF" -> rates.BIF
        "BNB" -> rates.BNB
        "BRL" -> rates.BRL
        "BTC" -> rates.BTC
        "CAD" -> rates.CAD
        "CHF" -> rates.CHF
        "CNY" -> rates.CNY
        "CZK" -> rates.CZK
        "DKK" -> rates.DKK
        "DOT" -> rates.DOT
        "EGP" -> rates.EGP
        "ETH" -> rates.ETH
        "EUR" -> rates.EUR
        "FIL" -> rates.FIL
        "GBP" -> rates.GBP
        "GHS" -> rates.GHS
        "HKD" -> rates.HKD
        "HUF" -> rates.HUF
        "IDR" -> rates.IDR
        "ILS" -> rates.ILS
        "INR" -> rates.INR
        "IOT" -> rates.IOT
        "JPY" -> rates.JPY
        "KES" -> rates.KES
        "KZT" -> rates.KZT
        "LTC" -> rates.LTC
        "MXN" -> rates.MXN
        "MYR" -> rates.MYR
        "NEO" -> rates.NEO
        "NOK" -> rates.NOK
        "NZD" -> rates.NZD
        "OMG" -> rates.OMG
        "PHP" -> rates.PHP
        "PLN" -> rates.PLN
        "RON" -> rates.RON
        "RUB" -> rates.RUB
        "RWF" -> rates.RWF
        "SEK" -> rates.SEK
        "SGD" -> rates.SGD
        "THB" -> rates.THB
        "TRX" -> rates.TRX
        "TRY" -> rates.TRY
        "UAH" -> rates.UAH
        "UNI" -> rates.UNI
        "USD" -> rates.USD
        "XAF" -> rates.XAF
        "XAG" -> rates.XAG
        "XAU" -> rates.XAU
        "XLM" -> rates.XLM
        "XMR" -> rates.XMR
        "ZAR" -> rates.ZAR
        "ZEC" -> rates.ZEC
        "ZMW" -> rates.ZMW
        else -> null
    }
}