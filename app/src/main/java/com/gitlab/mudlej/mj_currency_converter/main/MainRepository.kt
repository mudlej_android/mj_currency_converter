package com.gitlab.mudlej.mj_currency_converter.main

import com.gitlab.mudlej.mj_currency_converter.data.models.CurrencyResponse
import com.gitlab.mudlej.mj_currency_converter.main.util.Resource


interface MainRepository {

    suspend fun getRates(base: String): Resource<CurrencyResponse>
}