package com.gitlab.mudlej.mj_currency_converter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.gitlab.mudlej.mj_currency_converter.databinding.ActivityMainBinding
import com.gitlab.mudlej.mj_currency_converter.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var preferences : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        preferences = getPreferences(Context.MODE_PRIVATE)
        loadPreferences(binding)

        // convert button handler
        binding.convertButton.setOnClickListener {
            val from = binding.fromSpinner.selectedItem.toString().substring(0, 3)
            val to = binding.toSpinner.selectedItem.toString().substring(0, 3)
            savePreferences(from, to)

            viewModel.convert(binding.amountEditText.text.toString(), from, to)
            binding.amountEditText.clearFocus()
        }

        // switch button handler
        binding.switchButton.setOnClickListener {
            val fromSpinnerPosition = binding.fromSpinner.selectedItemPosition
            binding.fromSpinner.setSelection(binding.toSpinner.selectedItemPosition)
            binding.toSpinner.setSelection(fromSpinnerPosition)
        }

        // text view reset after edit text loses focus
        binding.amountEditText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) binding.resultTextView.text = ""
        }

        lifecycleScope.launchWhenStarted {
            viewModel.conversion.collect { event ->
                when (event) {
                    is MainViewModel.CurrencyEvent.Success -> {
                        binding.progressBar.isVisible = false
                        binding.resultTextView.text = event.resultText
                        binding.resultTextView.setTextColor(Color.BLUE)
                    }
                    is MainViewModel.CurrencyEvent.Failure -> {
                        binding.progressBar.isVisible = false
                        binding.resultTextView.text = "Error"
                        binding.resultTextView.setTextColor(Color.RED)

                        Toast.makeText(this@MainActivity,
                            "${event.errorText} :(", Toast.LENGTH_LONG).show()
                        Log.e("MainActivity", "onCreate viewModel Error: ${event.errorText}")
                    }
                    is MainViewModel.CurrencyEvent.Loading -> {
                        binding.progressBar.isVisible = true
                    }
                    is MainViewModel.CurrencyEvent.Empty -> {
                        binding.amountEditText.hint = "0"
                    }
                    else -> Unit
                }
                Log.e("---------:", "onCreate: |" +
                        "${binding.fromSpinner.selectedItem.toString().substring(0, 3)}|", )
            }
        }
    }

    private fun savePreferences(from: String, to: String) {
        with (preferences.edit()) {
            putString("FROM", from)
            putString("TO", to)
            apply()
        }
    }

    private fun loadPreferences(binding: ActivityMainBinding) {
        val valuesArray = resources.getStringArray(R.array.currency_codes)

        val default = valuesArray[0]
        val from = preferences.getString("FROM", default)
        val to = preferences.getString("TO", default)

        val fromIndex = valuesArray.indexOfFirst { it.substring(0, 3) == from }
        val toIndex = valuesArray.indexOfFirst { it.substring(0, 3) == to }

        binding.apply {
            fromSpinner.setSelection(if (fromIndex == -1) 0 else fromIndex)
            toSpinner.setSelection(if (toIndex == -1) 0 else toIndex)
        }
    }
}