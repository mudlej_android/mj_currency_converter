package com.gitlab.mudlej.mj_currency_converter.data.models

data class CurrencyResponse (
    val timestamp: String,
    val base: String,
    val rates: Rates
)