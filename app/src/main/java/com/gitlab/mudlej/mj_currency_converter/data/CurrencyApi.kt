package com.gitlab.mudlej.mj_currency_converter.data

import android.content.res.Resources
import com.gitlab.mudlej.mj_currency_converter.BuildConfig
import com.gitlab.mudlej.mj_currency_converter.R
import com.gitlab.mudlej.mj_currency_converter.data.models.CurrencyResponse
import com.gitlab.mudlej.mj_currency_converter.di.AppModule
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface CurrencyApi {

    @Headers("api-key: ${AppModule.API_KEY}")
    @GET("/latest")
    suspend fun getRates(@Query("base") base: String) : Response<CurrencyResponse>

}