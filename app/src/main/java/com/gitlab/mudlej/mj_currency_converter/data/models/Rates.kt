package com.gitlab.mudlej.mj_currency_converter.data.models

import com.google.gson.annotations.SerializedName

class Rates(
    @SerializedName("ADA")
    val ADA: Double,
    @SerializedName("AED")
    val AED: Double,
    @SerializedName("AUD")
    val AUD: Double,
    @SerializedName("BAT")
    val BAT: Double,
    @SerializedName("BDT")
    val BDT: Double,
    @SerializedName("BGN")
    val BGN: Double,
    @SerializedName("BIF")
    val BIF: Double,
    @SerializedName("BNB")
    val BNB: Double,
    @SerializedName("BRL")
    val BRL: Double,
    @SerializedName("BTC")
    val BTC: Double,
    @SerializedName("CAD")
    val CAD: Double,
    @SerializedName("CHF")
    val CHF: Double,
    @SerializedName("CNY")
    val CNY: Double,
    @SerializedName("CZK")
    val CZK: Double,
    @SerializedName("DKK")
    val DKK: Double,
    @SerializedName("DOT")
    val DOT: Double,
    @SerializedName("EGP")
    val EGP: Double,
    @SerializedName("ETH")
    val ETH: Double,
    @SerializedName("EUR")
    val EUR: Double,
    @SerializedName("FIL")
    val FIL: Double,
    @SerializedName("GBP")
    val GBP: Double,
    @SerializedName("GHS")
    val GHS: Double,
    @SerializedName("HKD")
    val HKD: Double,
    @SerializedName("HUF")
    val HUF: Double,
    @SerializedName("IDR")
    val IDR: Double,
    @SerializedName("ILS")
    val ILS: Double,
    @SerializedName("INR")
    val INR: Double,
    @SerializedName("IOT")
    val IOT: Double,
    @SerializedName("JPY")
    val JPY: Double,
    @SerializedName("KES")
    val KES: Double,
    @SerializedName("KZT")
    val KZT: Double,
    @SerializedName("LTC")
    val LTC: Double,
    @SerializedName("MXN")
    val MXN: Double,
    @SerializedName("MYR")
    val MYR: Double,
    @SerializedName("NEO")
    val NEO: Double,
    @SerializedName("NOK")
    val NOK: Double,
    @SerializedName("NZD")
    val NZD: Double,
    @SerializedName("OMG")
    val OMG: Double,
    @SerializedName("PHP")
    val PHP: Double,
    @SerializedName("PLN")
    val PLN: Double,
    @SerializedName("RON")
    val RON: Double,
    @SerializedName("RUB")
    val RUB: Double,
    @SerializedName("RWF")
    val RWF: Double,
    @SerializedName("SEK")
    val SEK: Double,
    @SerializedName("SGD")
    val SGD: Double,
    @SerializedName("THB")
    val THB: Double,
    @SerializedName("TRX")
    val TRX: Double,
    @SerializedName("TRY")
    val TRY: Double,
    @SerializedName("UAH")
    val UAH: Double,
    @SerializedName("UNI")
    val UNI: Double,
    @SerializedName("USD")
    val USD: Double,
    @SerializedName("XAF")
    val XAF: Double,
    @SerializedName("XAG")
    val XAG: Double,
    @SerializedName("XAU")
    val XAU: Double,
    @SerializedName("XLM")
    val XLM: Double,
    @SerializedName("XMR")
    val XMR: Double,
    @SerializedName("ZAR")
    val ZAR: Double,
    @SerializedName("ZEC")
    val ZEC: Double,
    @SerializedName("ZMW")
    val ZMW: Double,
) {
}