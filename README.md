<div align="center">
  <img src="https://gitlab.com/mudlej_android/mj_currency_converter/-/raw/main/app/src/main/res/mipmap-xxxhdpi/ic_launcher.png" width="160" />
</div>

# MJ Currency Converter

This is a very minimalist and simple currency converter app.
It's completely private and free and contains no ads.

## Screenshots

| Light Mode | Dark Mode |
|:-:|:-:|
<img src ="https://gitlab.com/mudlej_android/mj_currency_converter/-/raw/main/screenshots/ligth_framed.png" width="190" height="400"/> | <img src ="https://gitlab.com/mudlej_android/mj_currency_converter/-/raw/main/screenshots/dark_colored.png" width="190" height="400"/> |

## MJ Currency Converter Features
* Fast & smooth experience.
* Has a Very Minimalist and simple user interface.
* Supports Dark mode.
* Remembers the last choice of currencies.
* FOSS and totally private.
* Free of cost and ads. (made for learning)

## Privacy Policy
The app doesn't collect any data.
However, it uses https://xchangeapi.com/ API to get up-to-date currency rates.

If they are collecting data, they should not know more than your IP address.
- The app doesn't reveal your location. (since it doesn't have it)
- It doesn't tell the server how much you are trying to convert.
- It doesn't tell it the currency you are converting from or to.

## License
MJ Currency Converter uses the GPLv3 license.

